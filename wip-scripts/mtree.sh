#calomel.org  ids.sh
#

if [ $# -eq 0 ]
   then
    echo ""
    echo "  Calomel.org    ./ids.sh \$arg"
    echo "--------------------------------------"
    echo "generate = generate IDS signatures"
    echo "verify   = verify files against known signatures"
    echo ""
   exit
 fi

## mtree binary (OpenBSD: mtree and Linux: freebsd-mtree)
MTREE=mtree

## IDS seed signature key
KEY=12345946598234534539234

## IDS signature directory
DIR=/etc/ids_dir

if [ $1 = "generate" ]
   then
     rm -rf $DIR/mtree_*
     cd $DIR
     $MTREE -c -K cksum,md5digest,sha1digest, -s $KEY -p /bin > mtree_bin
     $MTREE -c -K cksum,md5digest,sha1digest, -s $KEY -p /sbin > mtree_sbin
     $MTREE -c -K cksum,md5digest,sha1digest, -s $KEY -p /usr/bin > mtree_usr_bin
     $MTREE -c -K cksum,md5digest,sha1digest, -s $KEY -p /usr/lib > mtree_usr_lib
     $MTREE -c -K cksum,md5digest,sha1digest, -s $KEY -p /usr/include > mtree_usr_include
     $MTREE -c -K cksum,md5digest,sha1digest, -s $KEY -p /usr/local/bin > mtree_usr_local_bin
     $MTREE -c -K cksum,md5digest,sha1digest, -s $KEY -p /usr/local/lib > mtree_usr_local_lib
     $MTREE -c -K cksum,md5digest,sha1digest, -s $KEY -p /usr/local/include > mtree_usr_local_include
     logger IDS generate IDS signatures
     chmod 600 $DIR/mtree_*   
   exit
 fi

if [ $1 = "verify" ]
   then
     cd $DIR
     $MTREE -s $KEY -p /bin < mtree_bin >> temp 2>&1
     $MTREE -s $KEY -p /sbin < mtree_sbin >> temp 2>&1
     $MTREE -s $KEY -p /usr/bin < mtree_usr_bin >> temp 2>&1
     $MTREE -s $KEY -p /usr/lib < mtree_usr_lib >> temp 2>&1
     $MTREE -s $KEY -p /usr/include < mtree_usr_include >> temp 2>&1
     $MTREE -s $KEY -p /usr/local/bin < mtree_usr_local_bin >> temp 2>&1
     $MTREE -s $KEY -p /usr/local/lib < mtree_usr_local_lib >> temp 2>&1
     $MTREE -s $KEY -p /usr/local/include < mtree_usr_local_include >> temp 2>&1     
     cat temp | mail -s "`hostname` file integrity check" root
     cat temp > /etc/temp
     rm temp
     logger IDS verify files against known signatures
   exit
 fi
